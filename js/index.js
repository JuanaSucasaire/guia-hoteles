$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 3000
    });

    $('#contacto').on('show.bs.modal', function(e) {
      console.log("El modal se esta mostrando");

      $('#contactoBtn').removeClass('btn-success');
      $('#contactoBtn').addClass('btn-primary');

      //Para desabilitar un boton
      $('#contactoBtn').prop('disabled', true);
    })
    $('#contacto').on('shown.bs.modal', function(e) {
      console.log("El modal se mostró");
    })
    $('#contacto').on('hide.bs.modal', function(e) {
      console.log("El modal se oculta");
    })
    $('#contacto').on('hidden.bs.modal', function(e) {
      console.log("El modal se ocultó");

      //Para volver a habilitar el boton
      $('#contactoBtn').prop('disabled', false);
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-success');
    })
  
})